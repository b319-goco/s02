function factorial(n) {
	if(typeof n !== 'number') return undefined; // to check value if type is number
	if(n<0) return undefined; // to check if the value is not negative
	if(n===0) return 1;  // to check if the value is 0
	if(n===1) return 1; // to check if the value is 1
	return n * factorial(n-1); // for the rest of the numbers >1
}

function div_check(n) {
	if(typeof n !== 'number') return undefined;
	if(n%5===0|| n%7===0) return true
	else return false
	
}

module.exports = {
	factorial: factorial,
	div_check: div_check
}