const { factorial, div_check } = require('../src/util.js');
const { expect, assert } = require('chai');

describe('test_fun_factorials', () => {
	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0)
		assert.equal(product,1,"The factorial of 0 is equal to 1");
	})
	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4)
		assert.equal(product,24,"The factorial of 4 is equal to 24");
	})
	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10)
		assert.equal(product,3628800,"The factorial of 10 is equal to 3628800");
	})
})

describe('test_fun_div_check_by_5_or_7', () => {
	it('test_fun_div_check_10_by_5_or_7', () => {
		const product = div_check(10);
		assert.equal(product,true,"10 is divisible by 5");
	})
	it('test_fun_div_check_21_by_5_or_7', () => {
		const product = div_check(21);
		assert.equal(product,true,"21 is divisible by 7");
	})
	it('test_fun_div_check_35_by_5_or_7', () => {
		const product = div_check(35);
		assert.equal(product,true,"35 is divisible by 5 and 7");
	})
	it('test_fun_div_check_31_by_5_or_7', () => {
		const product = div_check(31);
		assert.equal(product,false,"31 is NOT divisible by 5 and 7");
	})
})

